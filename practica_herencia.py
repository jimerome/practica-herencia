class Persona:
    def __init__(self, nombre, apellidos, fecha_nacimiento, dni):
        self.nombre = nombre
        self.apellidos = apellidos
        self.fecha_nacimiento = fecha_nacimiento
        self.dni = dni
   
    def __str__(self):
        return f"El/La paciente {self.nombre}\n de apellido {self.apellidos}\nnacido en la fecha {self.fecha_nacimiento}\n y con dni {self.dni}"
   
    # SETTERS Y GETTERS A CONTINUACIÓN: 
    def get_nombre(self):
        return self.nombre
    def set_nombre(self, nombre):
        self.nombre = nombre
   
    def get_apellidos(self):
        return self.apellidos
    def set_apellidos(self, apellidos):
        self.apellidos = apellidos   
   
    def get_fecha_nacimiento(self):
        return self.fecha_nacimiento
    def set_fecha_nacimiento(self, fecha_nacimiento):
        self.fecha_nacimiento = fecha_nacimiento
   
    def get_dni(self):
        return self.dni
    def set_dni(self, dni):
        self.dni = dni

#INTRODUCIMOS LAS CLASES QUE DEPENDEN DE PERSONA: PACIENTE Y MÉDICO:
class Paciente(Persona):
    def __init__(self, nombre, apellidos, fecha_nacimiento, dni, historial_clinico):
        super().__init__(nombre, apellidos, fecha_nacimiento, dni)
        self.historial_clinico = historial_clinico
   
    def ver_historial_clinico(self):
        return self.historial_clinico

class Medico(Persona):
    def __init__(self, nombre, apellidos, fecha_nacimiento, dni, especialidad):
        super().__init__(nombre, apellidos, fecha_nacimiento, dni)
        self.especialidad = especialidad
        self.citas = []
   
    def consulta_agenda(self):
        if not self.citas:
            return "Usted no tiene citas programadas."
        else:
            agenda = "Agenda de citas:\n"
            for cita in self.citas:
                agenda += f"- {cita}\n"
            return agenda