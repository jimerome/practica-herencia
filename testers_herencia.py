import unittest
from practica_herencia import Persona
from practica_herencia import Paciente
from practica_herencia import Medico


class TestPersona(unittest.TestCase):
    paciente_ejemplo = Paciente("Sonia", "Fernandez", "09/11/1975", "02754982A", "Sufre de cólicos renales. Operación de vegetaciones en 2015. En tratamiento dermatológico, actualmente, por acne.")
    print(paciente_ejemplo)
    print("Historial Clínico:")
    print(paciente_ejemplo.ver_historial_clinico())

    print("\n")

    medico_ejemplo = Medico("Begoña", "Castaños", "14/03/2000", "05612342T", "Cardiología")
    medico_ejemplo.citas = ["La Dra. Castaños tiene consulta el 16/08/2024 a la 13:00", "La Dra.Castaños tiene programada una cirujía el 30/08/2024 a las 09:30"]
    print(medico_ejemplo)
    print(medico_ejemplo.consulta_agenda())

if __name__ == '__main__':
    unittest.main()

